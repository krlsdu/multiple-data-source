package guru.springframework.multipledatasources;

import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import guru.springframework.multipledatasources.basecorrente.model.CorrenteVeiculo;
import guru.springframework.multipledatasources.basecorrente.model.CorrenteRestricao;
import guru.springframework.multipledatasources.basecorrente.repository.VeiculoRepository;
import guru.springframework.multipledatasources.basecorrente.repository.RestricaoRepository;
import guru.springframework.multipledatasources.model.card.Card;
import guru.springframework.multipledatasources.model.cardholder.CardHolder;
import guru.springframework.multipledatasources.model.member.Member;
import guru.springframework.multipledatasources.repository.card.CardRepository;
import guru.springframework.multipledatasources.repository.cardholder.CardHolderRepository;
import guru.springframework.multipledatasources.repository.member.MemberRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MultipledatasourcesApplicationTests {

    /*
    * We will be using mysql databases we configured in our properties file for our tests
    * Make sure your datasource connections are correct otherwise the test will fail
    * */

    @Autowired
    private VeiculoRepository carRepository;

    @Autowired
    private RestricaoRepository restricaoRepository;
    
    @Autowired
    private MemberRepository memberRepository;

    @Autowired
    private CardHolderRepository cardHolderRepository;

    @Autowired
    private CardRepository cardRepository;

    private CorrenteVeiculo car;
    private CorrenteRestricao restricao;
    private Member member;
    private Card card;
    private CardHolder cardHolder;

    @Before
    public void initializeDataObjects(){

        member = new Member();
        member.setMemberId("M001");
        member.setName("Maureen Mpofu");

        cardHolder = new CardHolder();
        cardHolder.setCardNumber("4111111111111111");
        cardHolder.setMemberId(member.getMemberId());

        card = new Card();
        card.setExpirationMonth(01);
        card.setExpirationYear(2020);
        card.setName(member.getName());
        
        car = new CorrenteVeiculo();
        car.setIt_nu_ident_unica_veiculo("47856979275");

        restricao = new CorrenteRestricao();
        restricao.setIt_nu_ident_unica_veiculo("47856979275");

    }

    @Test
    public void shouldSaveRestricaoToBaseCorrenteDB() {
        CorrenteRestricao savedRestricao =restricaoRepository.save(restricao);
        List<CorrenteRestricao> restricaoFromDb=restricaoRepository.findAll();
        assertTrue(restricaoFromDb.get(0).equals(restricao));
    }
    
    @Test
    public void shouldSaveCarToCarDB() {
        CorrenteVeiculo savedCar =carRepository.save(car);
        List<CorrenteVeiculo> carFromDb= carRepository.findAll();
        assertTrue(carFromDb.get(0).equals(car));
    }
    
    @Test
    public void shouldSaveMemberToMemberDB() {
        Member savedMember =memberRepository.save(member);
        Optional<Member> memberFromDb= memberRepository.findById(savedMember.getId());
        assertTrue(memberFromDb.isPresent());
    }

    @Test
    public void shouldSaveCardHolderToCardHolderDB() {
        CardHolder savedCardHolder =cardHolderRepository.save(cardHolder);
        Optional<CardHolder> cardHolderFromDb= cardHolderRepository.findById(savedCardHolder.getId());
        assertTrue(cardHolderFromDb.isPresent());
    }

    @Test
    public void shouldSaveCardToCardDB() {
        Card savedCard = cardRepository.save(card);
        Optional<Card> cardFromDb= cardRepository.findById(savedCard.getId());
        assertTrue(cardFromDb.isPresent());
    }
}

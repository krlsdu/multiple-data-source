package guru.springframework.multipledatasources.basecorrente.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import guru.springframework.multipledatasources.basecorrente.model.CorrenteRestricao;

public interface RestricaoRepository extends JpaRepository<CorrenteRestricao, Long> {
}

package guru.springframework.multipledatasources.basecorrente.configuration;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.zaxxer.hikari.HikariDataSource;


@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(basePackages = "guru.springframework.multipledatasources.basecorrente.repository",
        entityManagerFactoryRef = "baseCorrenteEntityManagerFactory",
        transactionManagerRef= "baseCorrenteTransactionManager"
)
public class BaseCorrenteDataSourceConfiguration {

    @Bean
    @ConfigurationProperties("app.datasource.car")
    public DataSourceProperties carDataSourceProperties() {
        return new DataSourceProperties();
    }

    @Bean
    @ConfigurationProperties("app.datasource.car.configuration")
    public DataSource baseCorrenteDataSource() {
        return carDataSourceProperties().initializeDataSourceBuilder()
                .type(HikariDataSource.class).build();
    }

    @Bean(name = "baseCorrenteEntityManagerFactory")
    public LocalContainerEntityManagerFactoryBean carEntityManagerFactory(EntityManagerFactoryBuilder builder) {
        return builder
                .dataSource(baseCorrenteDataSource())
                .packages("guru.springframework.multipledatasources.basecorrente")
                .build();
    }

    @Bean
    public PlatformTransactionManager baseCorrenteTransactionManager(
            final @Qualifier("baseCorrenteEntityManagerFactory") LocalContainerEntityManagerFactoryBean baseCorrenteEntityManagerFactory) {
        return new JpaTransactionManager(baseCorrenteEntityManagerFactory.getObject());
    }

}

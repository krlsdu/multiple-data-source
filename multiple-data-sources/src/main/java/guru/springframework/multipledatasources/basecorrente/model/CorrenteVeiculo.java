package guru.springframework.multipledatasources.basecorrente.model;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import lombok.Data;

@Data
@Entity
@Table(name = "corrente_veiculo")
public class CorrenteVeiculo {

	@Id
	private String it_nu_ident_unica_veiculo;

	@Column
	private String it_nu_renavam_veiculo;

	@Column
	private String it_nu_placa_veiculo;

	@Column
	private String it_nu_identificacao_veiculo;
	
	@Column
	private LocalDateTime it_da_ho_atualizacao;

	@Column
	private Integer it_in_ativo;

	@Column
	private String it_nu_sequencial_docto_crv;

	@Column
	private String it_nu_crv;

	@Column
	private LocalDate it_da_emissao_crv;

	@Column
	private LocalTime it_ho_emissao_crv;

	@Column
	private String it_nu_via_crv;

	@Column
	private String it_co_seguranca_crv;

	@Column
	private String it_nu_motor_veiculo;

	@Column
	private String it_nu_caixa_cambio_veiculo;

	@Column
	private String it_nu_eixo_traseiro_veiculo;

	@Column
	private String it_nu_eixo_auxiliar_veiculo;

	@Column
	private String it_nu_carroceria_veiculo;

	@Column
	private String it_co_tipo_situacao_veiculo;

	@Column
	private String it_sg_uf_jurisdicao_veiculo;

	@Column
	private String it_co_municipio_domi_prop_veic;

	@Transient
	private String descricaoMunicipioEmplacamento;

	@Column
	private String it_co_marca_modelo_veiculo;

	@Transient
	private String descricaoMarcaModelo;

	@Column
	private String it_co_cor_veiculo;

	@Transient
	private String descricaoCor;

	@Column
	private Integer it_an_fabricacao_veiculo;

	@Column
	private Integer it_an_modelo_veiculo;

	@Column
	private String it_co_combustivel_veiculo;

	@Transient
	private String descricaoCombustivel;

	@Column
	private String it_co_tipo_veiculo;

	@Transient
	private String descricaoTipoVeiculo;

	@Column
	private String it_co_especie_veiculo;

	@Transient
	private String descricaoEspecie;

	@Column
	private String it_co_categoria_veiculo;

	@Transient
	private String descricaoCategoria;

	@Column
	private Integer it_qt_potencia_veiculo;

	@Column
	private Integer it_qt_cilindrada_veiculo;

	@Column
	private String it_co_tipo_carroceria_veiculo;

	@Transient
	private String descricaoCarroceria;

	@Column
	private Double it_qt_capacidade_maxi_trac_veic;

	@Column
	private Double it_qt_peso_bruto_veiculo;

	@Column
	private Double it_qt_capacidade_maxi_carga_veic;

	@Column
	private Integer it_qt_passageiro_veiculo;

	@Column
	private Integer it_qt_eixo_veiculo;

	@Column
	private String it_in_veiculo_nacional;

	@Column
	private String it_co_tipo_doc_propr;

	@Column
	private String it_nu_identificacao_propr;

	@Column
	private String it_no_propr;

	@Column
	private String it_no_logradouro_propr;

	@Column
	private String it_nu_logradouro_propr;

	@Column
	private String it_no_complemento_logr_propr;

	@Column
	private String it_no_bairro_propr;

	@Column
	private String it_co_municipio_propr;

	@Transient
	private String descricaoMunicipioProprietario;

	@Column
	private String it_sg_uf_imovel_propr;

	@Column
	private String it_nu_cep_logradouro_propr;

	@Column
	private String it_co_tipo_doc_arrend;

	@Column
	private String it_nu_identificacao_arrend;

	@Column
	private String it_no_arrend;

	@Column
	private String it_no_logradouro_arrend;

	@Column
	private String it_nu_logradouro_arrend;

	@Column
	private String it_no_complemento_logr_arrend;

	@Column
	private String it_no_bairro_arrend;

	@Column
	private String it_co_municipio_arrend;

	@Transient
	private String descricaoMunicipioArrendatario;

	@Column
	private String it_sg_uf_imovel_arrend;

	@Column
	private String it_nu_cep_logradouro_arrend;

	@Column
	private Integer it_in_detentor;

	@Column
	private String it_co_tipo_doc_detentor;

	@Column
	private String it_nu_identificacao_detentor;

	@Column
	private String it_no_detentor;

	@Column
	private String it_no_logradouro_detentor;

	@Column
	private String it_nu_logradouro_detentor;

	@Column
	private String it_no_complemento_logr_detentor;

	@Column
	private String it_no_bairro_detentor;

	@Column
	private String it_co_municipio_detentor;

	@Transient
	private String descricaoMunicipioDetentor;

	@Column
	private String it_sg_uf_imovel_detentor;

	@Column
	private String it_nu_cep_logradouro_detentor;

	@Column
	private String it_nu_sequencial_docto_crlv;

	@Column
	private String it_nu_tipografico_crlv;

	@Column
	private LocalDate it_da_emissao_crlv;

	@Column
	private LocalTime it_ho_emissao_crlv;

	@Column
	private Integer it_nu_via_crlv;

	// novos campos depois do projeto DPRF (consulta fiscalização)

	@Column
	private Integer it_co_restricao_veiculo_1;

	@Column
	private Integer it_co_restricao_veiculo_2;

	@Column
	private Integer it_co_restricao_veiculo_3;

	@Column
	private Integer it_co_restricao_veiculo_4;

	@Column
	private Integer it_in_restricoes_migradas;

	@Column
	private String it_nu_placa_pre_mercosul;

	@Column
	private String it_in_pre_cadastro;

	@Column
	private String it_in_tipo_montagem_veiculo;

	@Column
	private String it_co_montadora_principal_veic;
	
	@Transient
	private String descricaoMontadoraPrincipal;

	@Column
	private String it_co_montadora_org_pre_cadastro;
	
	@Transient
	private String descricaoMontadoraOrg;

	@Column
	private LocalDate it_da_pre_cadastro;

	@Column
	private Integer it_mes_fabricacao_veiculo;
	
	@Column
	private String it_nu_lcvm_lcm;
	
	@Column
	private String it_nu_lote_pre_cadastro;
	
	@Column
	private String it_in_remarcacao_chassi_veiculo;
	
	@Column
	private String in_natureza_faturado_veiculo;
	
	@Column
	private String it_nu_identificacao_faturado;
	
	@Column
	private String it_sg_uf_destino_veic_fatu_mont;
	
	@Column
	private LocalDate it_da_emplacamento;
	
	@Column
	private Integer it_co_trans_emplacamento;
	
	@Column
	private Integer it_co_ultima_transacao;
	
	@Column
	private Integer it_co_motivo_rest_trib;
	
	@Column
	private String it_in_dispensa_cat;
	
	@Column
	private Integer it_in_tipo_baixa;
	
	@Column
	private LocalDate it_da_dist_decl_impo_veic;
	
	@Column
	private String it_co_orgao_srf;
	
	@Column
	private String it_in_natureza_importador_veic;
	
	@Column
	private String it_nu_identificacao_impo_veic;
	
	@Column
	private String it_co_pais_transferencia_veiculo;
	
	@Column
	private String it_nu_declaracao_importacao_veic;
	
	@Column
	private String it_nu_processo_importacao_veic;
	
	@Column
	private String it_nu_registro_despacho_adua;
	
	@Column
	private Integer it_in_origem_dados_ampliados;
	
	@Column
	private Integer it_co_motivo_emissao_crv;
	
	@Column
	private String it_in_alteracao_detran;
	
	@Column
	private String it_in_alteracao_denatran;
	
	@Column
	private LocalDateTime it_da_hora_incl_dados_ampliados;
	
	@Column
	private Integer it_co_debito_ipva;
	
	@Column
	private Integer it_co_debito_licenciamento;
	
	@Column
	private Integer it_co_debito_multas;
	
	@Column
	private Integer it_co_debito_dpvat;
	
	@Column
	private Integer it_ano_ultimo_licenciamento;
	
	@Column
	private String it_mes_ano_validade_licenciament;
	
	@Column
	private Double it_va_ipva;
	
	@Column
	private Double it_va_licenciamento;
	
	@Column
	private Integer it_co_motivo_emissao_crlv;
	
	@Column
	private Integer it_co_origem_detentor;
	
	@Column
	private String it_co_origem_renavam;

	@Column
	private LocalDate it_da_limite_restricao_trib;
}
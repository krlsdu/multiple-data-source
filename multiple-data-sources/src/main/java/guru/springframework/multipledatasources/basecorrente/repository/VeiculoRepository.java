package guru.springframework.multipledatasources.basecorrente.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import guru.springframework.multipledatasources.basecorrente.model.CorrenteVeiculo;

public interface VeiculoRepository extends JpaRepository<CorrenteVeiculo, Long> {
}

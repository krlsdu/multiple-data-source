package guru.springframework.multipledatasources.basecorrente.model;

import java.io.Serializable;

import lombok.Data;

@Data
public class RestricoesPK implements Serializable {

    private static final long serialVersionUID = 933245850539768389L;

    private String it_nu_ident_unica_veiculo;

    private String it_co_tipo_restricao;
}
package guru.springframework.multipledatasources.basecorrente.model;

 import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "corrente_veiculo_restricoes")
@IdClass(RestricoesPK.class)
public class CorrenteRestricao {

    @Id
    @Column
    private String it_nu_ident_unica_veiculo;

    @Column
    private String it_nu_renavam_veiculo;

    @Column
    private String it_nu_placa_veiculo;

    @Column
    private String it_nu_identificacao_veiculo;

    @Column
    private Integer it_in_ativo;

    @Id
    @Column
    private String it_co_tipo_restricao;

    @Column
    private String it_co_subtipo_restricao;

    @Column
    private LocalDateTime it_da_ho_registro_renavam;
    
    @Column
    private String it_co_tipo_doc_indicado;

    @Column
    private String it_nu_identificacao_indicado;
    
    @Column
    private LocalDateTime it_da_ho_atualizacao;
}

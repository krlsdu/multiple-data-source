package guru.springframework.multipledatasources;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import guru.springframework.multipledatasources.basecorrente.model.CorrenteVeiculo;
import guru.springframework.multipledatasources.basecorrente.model.CorrenteRestricao;
import guru.springframework.multipledatasources.basecorrente.repository.VeiculoRepository;
import guru.springframework.multipledatasources.basecorrente.repository.RestricaoRepository;
import guru.springframework.multipledatasources.model.cardholder.CardHolder;
import guru.springframework.multipledatasources.model.member.Member;
import guru.springframework.multipledatasources.repository.cardholder.CardHolderRepository;
import guru.springframework.multipledatasources.repository.member.MemberRepository;

@SpringBootApplication
public class MultipledatasourcesApplication implements CommandLineRunner {

	@Autowired
	private MemberRepository memberRepository;
	@Autowired
	private CardHolderRepository cardHolderRepository;

	@Autowired
	private VeiculoRepository carRepository;
	
    private CorrenteRestricao restricao;
	

    @Autowired
    private RestricaoRepository restricaoRepository;

	private Member member;
	private CardHolder cardHolder;
	private CorrenteVeiculo car;

	public static void main(String[] args) {
		SpringApplication.run(MultipledatasourcesApplication.class, args);
	}

	@Override
	public void run(String... args) {

		member = new Member();
		member.setMemberId("M002");
		member.setName("Maureen Mpofu");
		memberRepository.save(member);

		cardHolder = new CardHolder();
		cardHolder.setCardNumber("4111111111111111");
		cardHolder.setMemberId(member.getMemberId());
		cardHolderRepository.save(cardHolder);

		car = new CorrenteVeiculo();
		car.setIt_nu_ident_unica_veiculo("47856979275");
		carRepository.save(car);
		
        restricao = new CorrenteRestricao();
        restricao.setIt_nu_ident_unica_veiculo("47856979275");
        restricao.setIt_co_tipo_restricao("judicial");
        restricaoRepository.save(restricao);
        
        
        restricao.setIt_nu_ident_unica_veiculo("5555555");
        restricao.setIt_co_tipo_restricao("restrito");
        restricaoRepository.save(restricao);

		System.out.println("\nfindAllMember()");
		memberRepository.findAll().forEach(x -> System.out.println(x));

		System.out.println("\nfindAllCardHolder()");
		cardHolderRepository.findAll().forEach(x -> System.out.println(x));

		System.out.println("\nfindAllCar()");
		carRepository.findAll().forEach(x -> System.out.println(x));
		
		System.out.println("\nfindAllRestricoes()");
		restricaoRepository.findAll().forEach(x -> System.out.println(x));
	}

}
